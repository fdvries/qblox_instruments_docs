|
|

.. figure:: https://cdn.sanity.io/images/ostxzp7d/production/f9ab429fc72aea1b31c4b2c7fab5e378b67d75c3-132x31.svg
    :width: 400px
    :target: https://qblox.com
    :align: center
    :alt: Qblox

|
|

.. image:: https://gitlab.com/qblox/packages/software/qblox_instruments_docs/badges/master/pipeline.svg
    :target: https://gitlab.com/qblox/packages/software/qblox_instruments_docs/pipelines/

.. image:: https://readthedocs.com/projects/qblox-qblox-instruments/badge/?version=master
    :target: https://qblox-qblox-instruments.readthedocs-hosted.com/en/master/?badge=master

.. image:: https://img.shields.io/badge/License-BSD%204--Clause-blue.svg
    :target: https://gitlab.com/qblox/packages/software/qblox_instruments/-/blob/master/LICENSE

|

##########################################
Welcome to Qblox Instruments Documentation
##########################################

| Qblox instruments documentation contains the documentation for the `Qblox instruments <https://gitlab.com/qblox/packages/software/qblox_instruments>`_ package.
| The published documentation can be found `here <https://qblox-qblox-instruments.readthedocs-hosted.com/en/master/>`_.

----------------------------

| This documentation and software is free to use under the conditions specified in the `license <https://gitlab.com/qblox/packages/software/qblox_instruments/-/blob/master/LICENSE>`_.
| For more information, please contact `support@qblox.com <support@qblox.com>`_.

----------------------------
