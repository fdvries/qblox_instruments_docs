.. include:: ../qblox_instruments/README.rst

########
Contents
########

    .. toctree::
        :maxdepth: 2
        :caption: Getting started
        :name: getting_started

        getting_started/overview.rst
        getting_started/handling.rst
        getting_started/servicing.rst
        getting_started/setup.rst
        getting_started/updating.rst
        getting_started/what_is_next.rst

    .. toctree::
        :maxdepth: 2
        :caption: Documentation
        :name: documentation

        documentation/general.rst
        documentation/pulsar.rst
        documentation/sequencer.rst
        documentation/synchronization.rst
        documentation/feedback.rst
        documentation/troubleshooting.rst

    .. toctree::
        :maxdepth: 1
        :caption: Tutorials
        :name: tutorials

        tutorials/cont_wave_mode.ipynb
        tutorials/basic_sequencing.ipynb
        tutorials/advanced_sequencing.ipynb
        tutorials/scope_acquisition.ipynb
        tutorials/binned_acquisition.ipynb
        tutorials/ttl_acquisition.ipynb
        tutorials/synchronization.ipynb
        tutorials/nco.ipynb
        tutorials/mixer_correction.ipynb
        tutorials/multiplexed_sequencing.ipynb
        tutorials/conditional_playback.ipynb
        tutorials/rf_control.ipynb
        tutorials/rabi_experiment.ipynb
        tutorials/spi_rack.ipynb

    .. toctree::
        :maxdepth: 1
        :caption: API reference
        :name: api_reference

        api_reference/pulsar.rst
        api_reference/cluster.rst
        api_reference/spi_rack.rst
        api_reference/qcm_qrm.rst
        api_reference/sequencer.rst
        api_reference/cfg_man.rst
        api_reference/pnp.rst
        api_reference/build_info.rst

#######
Indices
#######

* :ref:`genindex`
