.. _api_cfg_man:

Configuration management
========================

The following class allows you to manage Pulsar and Cluster instruments from within
Python.

.. note::
    There is also a command-line tool for this, that comes with the Qblox
    instruments, named ``qblox-cfg``. Run ``qblox-cfg help`` for its
    documentation.

.. autoclass:: qblox_instruments.ConfigurationManager

Supporting classes and functions
--------------------------------

.. automodule:: qblox_instruments.cfg_man.probe

.. automodule:: qblox_instruments.cfg_man.update_file
