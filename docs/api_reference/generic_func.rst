.. _api_generic_func:

Generic native interface functions
==================================

The native interface `generic_func` module contains the bulk of the functional code for the native interfaces
of the Pulsar and Cluster so that this code can be reused between instruments. The functions can be parametrized
using the :class:`~qblox_instruments.native.generic_func.FuncRefs` class that contains references to functions
of the native interfaces parent class.

.. automodule:: qblox_instruments.native.generic_func
