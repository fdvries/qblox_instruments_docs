.. _api_build_info:

Build information
=================

The following functions and classes allow you to programmatically retrieve version and build information for various components.

Information about Qblox instruments
-----------------------------------

.. autofunction:: qblox_instruments.get_build_info

.. autoclass:: qblox_instruments.BuildInfo

Information about an instrument
-------------------------------

.. autofunction:: qblox_instruments.get_device_info

.. autoclass:: qblox_instruments.DeviceInfo
