.. _api_instrument_types:

Instrument types
================

Instrument type enums and classes.

.. autoclass:: qblox_instruments.types.TypeEnum

.. autoclass:: qblox_instruments.InstrumentClass

.. autoclass:: qblox_instruments.InstrumentType

.. autoclass:: qblox_instruments.PulsarType

.. autoclass:: qblox_instruments.ClusterType

.. autoclass:: qblox_instruments.TypeHandle
