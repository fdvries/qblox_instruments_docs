.. _api_pnp:

Plug & Play
===========

The following class allows you to send Qblox Plug & Play commands from within
Python.

.. note::
    There is also a command-line tool for this, that comes with the Qblox
    instruments, named ``qblox-pnp``. Run ``qblox-pnp help`` for its
    documentation.

.. autoclass:: qblox_instruments.PlugAndPlay

Supporting classes and functions
--------------------------------

.. autofunction:: qblox_instruments.resolve

.. autoclass:: qblox_instruments.AddressInfo
