{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Mixer correction\n",
    "In this tutorial we will demonstrate the ability to compensate for output mixer non-idealities and observe the changes using an oscilloscope.\n",
    "\n",
    "Mixer non-idealities can lead to unwanted spurs on the output (LO/RF/IF feedthrough and other spurious products) and they can be compensated by applying adjustments to the I/Q outputs: phase offset, gain ratio and DC offset. This solution applies to both baseband QCM/QRM products using external mixers as well as QCM-RF and QRM-RF products.\n",
    "\n",
    "The tutorial is designed for all Qblox baseband products: Pulsar QRM, Pulsar QCM, Cluster QRM, Cluster QCM. We use the term 'QxM' encompassing both QCM and QRM modules. We will adjust all the parameters listed above and observe the changes to the I/Q outputs directly on an oscilloscope.\n",
    "\n",
    "For QCM-RF and QRM-RF products, one can refer to the 'mixer calibration' section of the tutorial on [RF-control](https://qblox-qblox-instruments.readthedocs-hosted.com/en/master/tutorials/rf_control.html#Mixer-calibration).\n",
    "\n",
    "To run this tutorial please make sure you have installed and enabled ipywidgets: \n",
    "```\n",
    "pip install ipywidgets\n",
    "jupyter nbextension enable --py widgetsnbextension\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Setup\n",
    "-----\n",
    "\n",
    "First, we are going to import the required packages."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import ipython widgets\n",
    "import json\n",
    "import math\n",
    "import os\n",
    "\n",
    "import ipywidgets as widgets\n",
    "import matplotlib.pyplot\n",
    "import numpy\n",
    "\n",
    "# Set up the environment.\n",
    "import scipy.signal\n",
    "from IPython.display import display\n",
    "from ipywidgets import fixed, interact, interact_manual, interactive\n",
    "\n",
    "from qblox_instruments import Cluster, PlugAndPlay, Pulsar"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Scan For Devices\n",
    "\n",
    "We scan for the available devices connected via ethernet using the Plug & Play functionality of the Qblox Instruments package (see [Plug & Play](https://qblox-qblox-instruments.readthedocs-hosted.com/en/master/api_reference/pnp.html) for more info)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The following widget displays all the existing modules that are connected to your PC which includes the Pulsar modules as well as a Cluster. Select the device you want to run the notebook on.\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "708e0bab4ce34df39cba625968398426",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Dropdown(description='Select Device', options=('pulsar-qrm', 'pulsar-qcm'), value='pulsar-qrm')"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Scan for available devices and display\n",
    "with PlugAndPlay() as p:\n",
    "    # get info of all devices\n",
    "    device_list = p.list_devices()\n",
    "    device_keys = list(device_list.keys())\n",
    "\n",
    "# create widget for names and ip addresses\n",
    "connect = widgets.Dropdown(\n",
    "    options=[(device_list[key][\"description\"][\"name\"]) for key in device_list.keys()],\n",
    "    description=\"Select Device\",\n",
    ")\n",
    "print(\n",
    "    \"The following widget displays all the existing modules that are connected to your PC which includes the Pulsar modules as well as a Cluster. Select the device you want to run the notebook on.\"\n",
    ")\n",
    "display(connect)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "#### Pulsar QxM <a id='pulsar_setup'></a>\n",
    "Run these cells after selecting the your Pulsar module. Skip to the [Cluster QxM section](#cluster_setup) below if you have selected a Cluster module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "pulsar-qrm connected at 192.168.0.4\n",
      "Status: OKAY, Flags: NONE, Slot flags: NONE\n"
     ]
    }
   ],
   "source": [
    "Pulsar.close_all()\n",
    "\n",
    "# Retrieve device name and IP address\n",
    "device_name = connect.value\n",
    "device_number = connect.options.index(device_name)\n",
    "ip_address = device_list[device_keys[device_number]][\"identity\"][\"ip\"]\n",
    "\n",
    "# Connect to device\n",
    "qxm = Pulsar(f\"{device_name}\", ip_address)\n",
    "qxm.reset()  # reset QxM\n",
    "print(f\"{device_name} connected at {ip_address}\")\n",
    "print(qxm.get_system_state())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Skip to the next section [(Setup Sequencer)](#setup_seq) if you are not using a cluster. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Cluster QxM <a id='cluster_setup'></a>\n",
    "First we connect to the Cluster using its IP address. Go to the [Pulsar QxM section](#pulsar_setup) if you are using a Pulsar.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# close all previous connections to the cluster\n",
    "Cluster.close_all()\n",
    "\n",
    "# Retrieve device name and IP address\n",
    "device_name = connect.value\n",
    "device_number = connect.options.index(device_name)\n",
    "ip_address = device_list[device_keys[device_number]][\"identity\"][\"ip\"]\n",
    "\n",
    "# connect to the cluster and reset\n",
    "cluster = Cluster(device_name, ip_address)\n",
    "cluster.reset()\n",
    "print(f\"{device_name} connected at {ip_address}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We then find all available cluster modules to connect to them individually."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Find all QRM/QCM modules\n",
    "available_slots = {}\n",
    "for module in cluster.modules:\n",
    "    # if module is currently present in stack\n",
    "    if cluster._get_modules_present(module.slot_idx):\n",
    "        # check if QxM is RF or baseband\n",
    "        if module.is_rf_type:\n",
    "            available_slots[f\"module{module.slot_idx}\"] = [\"QCM-RF\", \"QRM-RF\"][\n",
    "                module.is_qrm_type\n",
    "            ]\n",
    "        else:\n",
    "            available_slots[f\"module{module.slot_idx}\"] = [\"QCM\", \"QRM\"][\n",
    "                module.is_qrm_type\n",
    "            ]\n",
    "\n",
    "# List of all QxM modules present\n",
    "connect_qxm = widgets.Dropdown(options=[key for key in available_slots.keys()])\n",
    "\n",
    "print(available_slots)\n",
    "# display widget with cluster modules\n",
    "print()\n",
    "print(\"Select the QxM module from the available modules in your Cluster:\")\n",
    "display(connect_qxm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we connect to the selected Cluster module. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "QCM connected\n",
      "Status: OKAY, Flags: NONE, Slot flags: NONE\n"
     ]
    }
   ],
   "source": [
    "# Connect to the cluster QxM module\n",
    "module = connect_qxm.value\n",
    "qxm = getattr(cluster, module)\n",
    "print(f\"{available_slots[connect_qxm.value]} connected\")\n",
    "print(cluster.get_system_state())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Setup Sequencer <a id='setup_seq'></a>\n",
    "-----\n",
    "\n",
    "The easiest way to view the influence of the mixer correction is to mix the NCO sin and cos with I and Q values of 1 (fullscale). The instrument output would be simple sinusoids with a 90deg phase offset and identical amplitude.\n",
    "\n",
    "We use sequencer 0 to set I and Q values of 1 (fullscle) using DC offset and we mix those with the NCO signals."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Program sequence we will not use.\n",
    "sequence = {\"waveforms\": {}, \"weights\": {}, \"acquisitions\": {}, \"program\": \"stop\"}\n",
    "with open(\"sequence.json\", \"w\", encoding=\"utf-8\") as file:\n",
    "    json.dump(sequence, file, indent=4)\n",
    "    file.close()\n",
    "qxm.sequencer0.sequence(sequence)\n",
    "\n",
    "# Program fullscale DC offset on I & Q, turn on NCO and enable modulation.\n",
    "qxm.sequencer0.offset_awg_path0(1.0)\n",
    "qxm.sequencer0.offset_awg_path1(1.0)\n",
    "qxm.sequencer0.nco_freq(10e6)\n",
    "qxm.sequencer0.mod_en_awg(True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Control sliders\n",
    "-----\n",
    "\n",
    "Create control sliders for the parameters described in the introduction. Each time the value of a parameter is updated, the sequencer is automatically stopped from the embedded firmware for safety reasons and has to be manually restarted.\n",
    "\n",
    "The sliders cover the valid parameter range. If the code below is modified to input invalid values, the firmware will not program the values.\n",
    "\n",
    "Please connect the I/Q outputs to an oscilloscope and set to trigger continuously on the I channel at 0V. Execute the code below, move the sliders and observe the result on the oscilloscope."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "ba4ffce8c64946ec9410b9f293b55986",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=0.0, description='offset_I', max=1.0, min=-1.0, step=0.01), Output()),…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "df7616c401214ee7ac4bac91f8953690",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=0.0, description='offset_Q', max=1.0, min=-1.0, step=0.01), Output()),…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "4636483e91e54f78b0e3f3ec9961e427",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=0.5, description='gain_ratio', max=2.0, min=0.5), Output()), _dom_clas…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "9556a98081094c808ff03f927434d05b",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=0.0, description='phase_offset', max=45.0, min=-45.0, step=1.0), Outpu…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "<function __main__.set_phase_offset(phase_offset)>"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def set_offset_I(offset_I):\n",
    "    qxm.out0_offset(offset_I)\n",
    "    qxm.arm_sequencer(0)\n",
    "    qxm.start_sequencer(0)\n",
    "\n",
    "\n",
    "def set_offset_Q(offset_Q):\n",
    "    qxm.out1_offset(offset_Q)\n",
    "    qxm.arm_sequencer(0)\n",
    "    qxm.start_sequencer(0)\n",
    "\n",
    "\n",
    "def set_gain_ratio(gain_ratio):\n",
    "    qxm.sequencer0.mixer_corr_gain_ratio(gain_ratio)\n",
    "    qxm.arm_sequencer(0)\n",
    "    qxm.start_sequencer(0)\n",
    "\n",
    "\n",
    "def set_phase_offset(phase_offset):\n",
    "    qxm.sequencer0.mixer_corr_phase_offset_degree(phase_offset)\n",
    "    qxm.arm_sequencer(0)\n",
    "    qxm.start_sequencer(0)\n",
    "\n",
    "\n",
    "interact(\n",
    "    set_offset_I, offset_I=widgets.FloatSlider(min=-1.0, max=1.0, step=0.01, start=0.0)\n",
    ")\n",
    "interact(\n",
    "    set_offset_Q, offset_Q=widgets.FloatSlider(min=-1.0, max=1.0, step=0.01, start=0.0)\n",
    ")\n",
    "interact(\n",
    "    set_gain_ratio,\n",
    "    gain_ratio=widgets.FloatSlider(min=0.5, max=2.0, step=0.1, start=1.0),\n",
    ")\n",
    "interact(\n",
    "    set_phase_offset,\n",
    "    phase_offset=widgets.FloatSlider(min=-45.0, max=45.0, step=1.0, start=0.0),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Stop\n",
    "----\n",
    "\n",
    "Finally, let's stop the sequencers and close the instrument connection. One can also display a detailed snapshot containing the instrument parameters before \n",
    "closing the connection by uncommenting the corresponding lines. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Stop sequencers.\n",
    "qxm.stop_sequencer()\n",
    "\n",
    "# Print sequencer status (should now say it is stopped).\n",
    "print(qxm.get_sequencer_state(0))\n",
    "\n",
    "# Uncomment the following to print an overview of the instrument parameters.\n",
    "# Print an overview of the instrument parameters.\n",
    "# print(\"Snapshot:\")\n",
    "# qxm.print_readable_snapshot(update=True)\n",
    "\n",
    "# Close the instrument connection.\n",
    "Pulsar.close_all()\n",
    "Cluster.close_all()"
   ]
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
