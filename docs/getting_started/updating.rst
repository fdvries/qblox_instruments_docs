.. _updating:

Updating
========

Instrument
----------

Firmware
^^^^^^^^

First, download the latest firmware for your instrument: `Cluster <https://gitlab.com/qblox/releases/cluster_releases/-/releases>`_ | `Pulsar QCM <https://gitlab.com/qblox/releases/pulsar_qcm_releases/-/releases>`_ | `Pulsar QRM <https://gitlab.com/qblox/releases/pulsar_qrm_releases/-/releases>`_.
The firmware can then be installed using the Qblox Configuration Manager tool ``qblox-cfg``, shipped with the ``qblox-instruments`` Python package.

.. admonition:: Installing Qblox Configuration Manager
    :class: note, dropdown

    The configuration manager shipped with any ``qblox-instruments`` version should be able to communicate with all firmware versions.
    If you nonetheless run into problems with it (in particular when downgrading), please *update* ``qblox-instruments``
    (make sure you have activated the correct environment using ``conda activate my-env-name``, see :ref:`getting_started/setup:Installation`):

    .. code-block:: console

        $ pip install --upgrade qblox-instruments

    In case of downgrading firmware of your instruments, afterwards,
    you may have to install an older version of ``qblox-instruments`` for it to be compatible with the firmware version you installed
    (see `driver changelog on PyPi <https://pypi.org/project/qblox-instruments/>`_):

    .. code-block:: console

        $ pip install qblox-instruments==<version>

    Note that we used to ship the configuration manager with each firmware release, rather than with ``qblox-instruments``.
    This is, however, an older version of the tool, that will not be able to communicate with instruments running newer firmware versions.

.. tip::

    In general, the Qblox Configuration Manager tool can do much more than what's listed on this page. Run ``qblox-cfg --help`` for more information.

Before and after the update, you can verify the firmware version of the instruments, using a terminal of your choice
(for ``qblox-cfg`` to be available, remember to activate the correct environment using ``conda activate my-env-name``, see :ref:`getting_started/setup:Installation`):

.. code-block:: console

    $ qblox-cfg  192.168.0.2 -v

Then execute the firmware update, replacing ``<filename>`` with the file you downloaded:

.. code-block:: console

    $ qblox-cfg 192.168.0.2 update <filename>

After executing this command, follow the instructions given by the Qblox Configuration Manager.
For tracking the progress of the update, in case of a Cluster, look at the status (S) LED of the CMM, it will (also see :ref:`documentation/general:Frontpanel LEDs`):

* Turn ``red``/``purple`` during the update of the CMM,
* ``yellow`` during the update of the other modules,
* and finally ``white`` when done.

In case of a Pulsar, the status (S) LED will go from ``yellow`` while in progress to ``green``/``blue`` when done.

.. admonition:: Tip: Updating all instruments on the network
    :class: tip, dropdown

    With this command you can list all instruments found on the network:

    .. code-block:: console

        $ qblox-pnp list

    The following command will update all instruments on the network, in case they are compatible with the update file specified:

    .. code-block:: console

        $ qblox-cfg -k all update <filename>

    The Qblox Configuration Manager will report that it failed if one of the instruments it found wasn't compatible,
    but (due to the ``-k`` flag, short for "keep going") it will try to apply the updates to all instruments,
    instead of stopping immediately after trying to update an incompatible instrument.

.. admonition:: Tip: Verbose and Non-interactive modes
   :class: tip, dropdown

    By default, the Qblox Configuration Manager will ask you to confirm that you really want to update after printing a summary of the operations it will be performing.
    You can get more information by adding ``-v`` or ``-vv`` to the command line (or by inspecting the log file).

    Alternatively, if you want to automate ``qblox-cfg``, you may want to disable the prompt (a.k.a. non-interactive mode), which you can do with ``-y``.

IP address and name
^^^^^^^^^^^^^^^^^^^

.. admonition:: Default IP address
    :class: note

    The default IP address of the instrument is ``192.168.0.2``. Replace the IP address in any instruction listed on this page if the instrument's IP address was changed.
    To find the IP address of your instrument, see :ref:`getting_started/setup:Finding the IP address of a module`.

    When connected to your instrument(s) directly or via network switches only (**NOT** via VPN or a router),
    you can instead use the instrument name or serial number to select an instrument.

You can use the Qblox Configuration Manager to update the IP address of your instrument
(for ``qblox-cfg`` to be available, remember to activate the correct environment using ``conda activate my-env-name``, see :ref:`getting_started/setup:Installation`):

.. code-block:: console

    $ qblox-cfg 192.168.0.2 set-ip <new-ip-address>

.. admonition:: Tip: DHCP and IPv6
    :class: tip, dropdown

    Instruments can be configured to use DHCP instead of static addresses, or use IPv6 instead of (only) IPv4 addresses. Run ``qblox-cfg --help`` for more information.

Or to change its name:

.. code-block:: console

    $ qblox-cfg 192.168.0.2 set-name <new-name>

You can perform several operations at once, should you want to. For example, this will set the IP address and update the firmware of the instrument (originally) at ``192.168.0.2``:

.. code-block:: console

    $ qblox-cfg 192.168.0.2 set-ip <new-ip-address> update <filename>

After the update is completed, verify firmware version via:

.. code-block:: console

    $ qblox-cfg <new-ip-address> -v

Host PC
-------

Qblox Instruments
^^^^^^^^^^^^^^^^^

To update your installation of the ``qblox-instruments`` driver package, run
(make sure you have activated the correct environment using ``conda activate my-env-name``, see :ref:`getting_started/setup:Installation`):

.. code-block:: console

    $ pip install --upgrade qblox-instruments

Please make sure that the driver version you install is compatible with your Cluster or Pulsar firmware,
verify via the `driver changelog on PyPi <https://pypi.org/project/qblox-instruments/>`_.

Quantify
^^^^^^^^

If you have ``quantify-core`` or ``quantify-scheduler`` installed, a new version of Qblox Instruments may require you to update these as well:

.. code-block:: console

    $ pip install --upgrade qblox-instruments quantify-core quantify-scheduler
