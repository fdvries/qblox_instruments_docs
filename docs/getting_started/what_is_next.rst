.. _whats_next:

What's next
===========


Quantify
--------

To get the most out of your Qblox instruments, we advise that you install `Quantify`_; our measurement control package with built-in support for Qblox instruments. 
You can do this by following `the installation guide <https://quantify-quantify-core.readthedocs-hosted.com/en/latest/installation.html>`_.

Please consult `Quantify`_'s documentation for more information and tutorials.


Learn more
----------

If you want to learn more about our instruments, we advise that you read on. The following pages consist of documentation and tutorials for both beginning and advanced users.
The pages will go into more detail about the internal operations of the instruments and will teach you how to operate our instruments effectively and efficiently.

.. _Quantify: https://quantify-quantify-core.readthedocs-hosted.com/en/latest/