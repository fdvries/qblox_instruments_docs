.. _servicing:

Servicing
=========

Qblox instruments are sensitive pieces of equipment, which under no circumstances should be opened. The Cluster contains
high-voltage parts which are dangerous. The end user shall not install substitute parts or perform any modification to the
product. Opening the instruments immediately voids the warranty provided by Qblox.

Power
-----

In case the DC adapter of the Pulsar needs a replacement it should be replaced by a Meanwell SGA60E12-P1J (or
SGA60U12-P1J for the US).

.. note::
    The supplied mains power cord of the cluster should be used with the product. In case the cord becomes damaged it
    should always be replaced with a cord of the same or better ratings.

Batteries
---------

All Cluster modules are equipped with a CR2016 battery. The CR2016 batteries of the Cluster modules can be replaced by
the end user. To replace a CR2016 battery: extract the module from the Cluster, remove the old battery at the bottom side
of the module, and insert the new battery. After replacing the battery the module can be inserted back into the Cluster.

The Pulsar is not meant to be opened. In case he internal battery needs to be replaced, please contact Qblox.

.. note::
    Beware to handle the boards in an ESD safe manner!

Fuses
-----

The Cluster is double fused and these fuses can be found in the mains socket. To extract the fuses no tooling is needed
an the fuse tray can be extracted by hand.

The rating of the fuses used in the Clusters mains socket are dependent on the rated voltage of the mains network. For a
110 Volt supply a 10A fuse must be used. For a 230 Volt supply the 5A fuse must be used. For all ratings a time-delayed
fuse must be used.

.. note::
    For voltages between 110 Volt and 230 Volt the rating can be linearly interpolated.

..
    NOTE: Add all information regarding user serviceable parts to this section. Also the place to add information on
    removing/adding/replacing a module.
