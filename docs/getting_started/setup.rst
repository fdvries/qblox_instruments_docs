.. _setup:

Setup
=====

In this page, we are going to look at the installation steps for the various tools needed to operate Qblox hardware.
In addition, we will explain how to connect a `Qblox cluster <https://www.qblox.com/cluster>`_, `Pulsar module <https://www.qblox.com/pulsar>`_ or `SPI rack <https://www.qblox.com/spi>`_ to your host PC.
Qblox Clusters and Pulsars will work out of the box with the ``qblox-instruments`` Python driver.
We will take you through the installation steps for creating a virtual programming environment for Python and installing the ``qblox-instruments``
driver package, along with `Jupyter <https://jupyter.org/>`_ (required to run the tutorials) and :ref:`getting_started/setup:Quantify`, a high-level Python framework to carry out quantum computing experiments.

Installation
------------

In this section we will explain how to install the ``qblox-instruments`` package and set it up for use with Qblox hardware.

Qblox Instruments requires `Python 3.8 or newer <https://pypi.org/project/qblox-instruments/>`_. We recommend using Python 3.9, the latest version supported by Quantify currently (see `quantify-core <https://pypi.org/project/quantify-core/>`_ | `quantify-scheduler <https://pypi.org/project/quantify-scheduler/>`_).

.. admonition:: Tip for new Python users

    For users new to Python, we recommend using `Anaconda <https://www.anaconda.com/>`_ to set up your programming environment.

    Please follow the installation instructions provided by Anaconda, and open up an Anaconda prompt when the installation is finished. We will start by creating a new environment, which we will use to install packages independently from other conda or Python environments. We can specify the name and the Python version of the environment.
    Type the following in Anaconda prompt (we chose `my-env-name` as the name for the environment which could be replaced with another name of your choice):

    .. code-block:: console

        $ conda create -n my-env-name python=3.9
    
    Then activate the environment by typing ``conda activate my-env-name``. You are now ready to proceed with the next steps.
    For a quick overview of useful conda commands, we recommend going through the `conda cheat sheet <https://docs.conda.io/projects/conda/en/latest/_downloads/843d9e0198f2a193a3484886fa28163c/conda-cheatsheet.pdf>`_.

System dependencies
^^^^^^^^^^^^^^^^^^^

Qblox Instruments has one important system dependency, namely the HDF5 library. If this library is not installed on your system yet, please follow the installation instructions on the `HDF5 website <https://www.hdfgroup.org/downloads/hdf5/>`_.

New users of HDF5 should download the latest version for their operating system under the section `Pre-built Binary Distributions`.

.. note:: 
    (Windows only) If, after installation of the HDF5 library, you run into errors regarding Visual Studio C++, please install `Microsoft C++ Build Tools <https://visualstudio.microsoft.com/visual-cpp-build-tools/>`_ as well.

Qblox Instruments
^^^^^^^^^^^^^^^^^

The ``qblox-instruments`` driver package can be installed through `pip <https://pip.pypa.io/en/stable/>`_, by executing the following command (make sure you have activated the correct environment using ``conda activate my-env-name``):

.. code-block:: console


    $ pip install qblox-instruments

This will install the most recent version of the driver package. Please make sure that the driver version you install is compatible with your Cluster or Pulsar firmware,
verify via the `driver changelog on PyPi <https://pypi.org/project/qblox-instruments/>`_.
For information on the firmware and driver updating procedure, see section :ref:`getting_started/updating:Updating`.

To install a specific version of the driver package, execute the following command:

.. code-block:: console

    $ pip install qblox-instruments==<version>

.. tip::
    You can query your installed version by executing ``$ pip show qblox-instruments``.

With ``qblox-instruments``, two command-line tools will have been installed in your environment: :ref:`Qblox Configuration Manager <api_cfg_man>` and :ref:`Qblox Plug & Play <api_pnp>`.
We will be using these tools in the next step of connecting to your device(s): :ref:`connecting`.
In addition, you might have noticed the `QCoDeS <https://qcodes.github.io/Qcodes/>`_ dependency, which is the experiment and data acquisition framework used to control the Qblox hardware.

Jupyter-lab
^^^^^^^^^^^

Our tutorials are run on the interactive `ipython` platform called `Jupyter notebooks <https://jupyter.org/>`_. We recommend installing ``jupyter-lab`` which
provides a user-friendly interface to maintaining these notebooks. Following are the steps to install it:

* As always, start by activating the conda environment if you haven't already:

  .. code-block:: console

    $ # Replace my-env-name with the name of your environment
    $ conda activate my-env-name

* Install jupyter-lab from conda

  .. code-block:: console

    $ conda install -c conda-forge jupyterlab

* Once installed, run using

  .. code-block:: console

    $ jupyter-lab


Quantify
^^^^^^^^
Qblox provides an additional open-source software stack, `Quantify <https://quantify-os.org/>`_, to control experiments on Qblox hardware.
It is intended as a high-level interface to the Qblox hardware and contains frameworks to run quantum computing experiments from scratch.

.. note::
    Please make sure that you install Quantify in the same virtual environment as ``qblox-instruments`` (see :ref:`getting_started/setup:Installation`).

Quantify is built on top of `QCoDeS <https://qcodes.github.io/Qcodes/>`_ for handling various instruments and is split up into two packages:

- `quantify-core <https://pypi.org/project/quantify-core/>`_ | `installation and documentation <https://quantify-quantify-core.readthedocs-hosted.com/en/latest/installation.html>`__ is a data acquisition platform offering easy measurement control and data handling,
- `quantify-scheduler <https://pypi.org/project/quantify-scheduler/>`_ | `installation and documentation <https://quantify-quantify-scheduler.readthedocs-hosted.com/en/latest/installation.html>`__ translates high-level descriptions of quantum gates or pulse sequences into programs that can be uploaded and run on Cluster or Pulsar devices.

Please refer to the linked pages above for more information and installation instructions.

 .. _connecting:

Connecting to a Cluster or Pulsar
---------------------------------

In this section we will explain how to connect a `Qblox Cluster <https://www.qblox.com/cluster>`_ or `Pulsar module <https://www.qblox.com/pulsar>`_ to your host PC.

.. figure:: /figures/cluster_overview.png
        :width: 876px
        :height: 528px
        :align: center
        :alt: .

Connecting to a single module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As an example, we will consider a setup composed of:

    * A laptop (host PC) with an Ethernet port,
    * A Qblox Cluster or Pulsar, configured to use the default IP configuration ``192.168.0.2/24``.

The following steps will allow you to successfully connect the cluster to your local network:

1. Connect the the Pulsar or Cluster to your host PC using an Ethernet cable. For a Cluster, the ethernet port is located on the Cluster Management Module (CMM), i.e. module 0, of the cluster.
#. Power up the device. The device is ready when all LEDs are on and no LEDs are red (see :ref:`documentation/general:Frontpanel LEDs`).
#. Configure the network adapter of the host PC, so that its IP address is within subnet ``192.168.0.X`` (where ``X`` is in a range from 3 to 254).
   Make sure the subnet mask is set to ``255.255.255.0``.

    .. admonition:: Common mistake
        :class: warning

        A common mistake is to configure your host machine's IP address to be the same as the device you want to control. This will cause IP address collision and you will not be able to connect to your device. It is recommended to choose a relatively high number in your subnet (e.g. ``192.168.0.200``) in order to avoid collisions.

    .. note::
        Configuration of a network adapter varies slightly between operating systems. See section :ref:`getting_started/network_adapter_cfg:Network adapter configuration` for a Windows, Linux and MacOS description.

    At this point your setup will look similar to the example setup in the figure below (left: Cluster, right: Pulsar):

    .. figure:: /figures/setup_cluster_pc.jpg
        :width: 666px
        :height: 500px
        :align: left
        :alt: A Qblox Cluster connected to a laptop using a USB Ethernet adapter.

    .. figure:: /figures/setup_single_module.jpg
        :width: 333px
        :height: 500px
        :align: right
        :alt: A Pulsar QCM module connected to a laptop using a USB Ethernet adapter.

    After a few seconds, your device should be present on the local network. You can verify this by executing the following command in a terminal of your choice.

    .. note::
        The default IP address of the modules is ``192.168.0.2``. Replace the IP address of any following instruction accordingly if the IP address of the module was ever changed.
        See section :ref:`getting_started/setup:Finding the IP address of a module` in case you do not know the IP address.

    .. code-block:: console

        $ ping 192.168.0.2 # Press Ctrl + C to terminate the program

    If successful, the output should be similar to the following example output:

    .. code-block:: console

        PING 192.168.0.2 (192.168.0.2): 56 data bytes
        64 bytes from 192.168.0.2: icmp_seq=0 ttl=64 time=0.396 ms
        64 bytes from 192.168.0.2: icmp_seq=1 ttl=64 time=0.232 ms
        64 bytes from 192.168.0.2: icmp_seq=2 ttl=64 time=0.261 ms

#. Finally, connect to the module from your host PC by running the following snippet in an interactive :ref:`Python <getting_started/setup:Installation>` shell or Jupyter Notebook:

    **Pulsar:**

    .. code-block:: python

        # Import driver
        from qblox_instruments import Pulsar

        # Connect to module
        control_module = Pulsar("QCM", "192.168.0.2")

    .. tip::
        Close the connection to the module using ``control_module.close()``.

    **Cluster:**

    .. code-block:: python

        # Import driver
        from qblox_instruments import Cluster

        # Connect to module
        cluster = Cluster("cluster", "192.168.0.2")

    .. tip::
        Close the connection to the module using ``cluster.close()``.

    .. toctree::
        :hidden:

        network_adapter_cfg.rst
        finding_ip_addr.rst

To verify that you are connected to the correct instrument, please see `Verifying which instrument you're connected to`_ below.

.. note::
        The :ref:`documentation/general:Frontpanel LEDs` provide a quick way to check the status of your system after connection and during experiments.
        In case of an unexpected LED color, please refer to the :ref:`troubleshooting`.
        section of the Qblox documentation website.

Connecting to multiple systems
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To be able to control multiple systems we need to follow the steps described above, except now:

- Instead of connecting a module directly to the Ethernet adapter of the host PC, we will connect all the modules and the host PC to the same network using, for example, an Ethernet switch.
- The IP address of the modules **must** be changed to avoid IP collisions. See section :ref:`getting_started/updating:IP address and name` for further instructions on updating the IP address of the modules.

As an example, we will consider a setup composed of:

    * A laptop (host PC) with an Ethernet port,
    * At least two Cluster or Pulsar systems,
    * A network switch.

The following figure shows the example setup:

.. figure:: /figures/setup_multiple_modules.jpg
    :width: 500px
    :align: center
    :alt: A Pulsar QCM and QRM module connected to a laptop over an Ethernet switch.

The following Python code lets us connect to the systems in the example setup:

.. code-block:: python

    # Import drivers
    from qblox_instruments import Pulsar

    # Connect to modules
    pulsar_qcm = Pulsar("qcm", "192.168.0.2")  # This module uses the default IP address.
    pulsar_qrm = Pulsar("qrm", "192.168.0.3")  # This module's IP address was changed.

.. note::
    When using multiple instruments, in your setup, you might need to synchronize the in- and outputs of these participating instruments. See section :ref:`Synchronization` to learn how to do this.

Finding the IP address of a module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The most robust way to address any instrument connected via Ethernet, is to configure it using a static IP address and then use that address directly. As we've seen in earlier sections, Pulsars and Clusters ship with IP address ``192.168.0.2`` by default, expecting to be in the ``192.168.0.0/24`` subnet. However, having to remember IP addresses is not very user-friendly, and if the instruments are to be connected through an existing network, static IP addresses may not even be an option: most networks use DHCP to automatically configure the IP addresses of the endpoints connected to it. In the latter case, ask your system administrator to be sure.

If you have to use DHCP, or just don't want to have to hardcode IP addresses into your notebooks, you have two options, but both put some requirements on your network infrastructure:

    - **Addressing instruments by their name or serial number using Qblox Plug & Play.**
      This will only work when your PC is in the same subnet as the instruments, which is the case if, e.g., you are in the same Local Area Network (LAN). It will *not* work if there is a router or VPN in between. You can find all instruments addressable in this way by executing ``qblox-pnp list``, which will show the IP addresses, names and serial numbers:

        .. code-block:: console

            $ qblox-pnp list
            Devices:
            - 192.168.0.3: Pulsar QRM 0.7.0 with name "test" and serial number 00013_2120_003

        Qblox Plug & Play is also available through its :ref:`Python API <api_pnp>`, which can be used as follows.

        .. code-block:: python

            from qblox_instruments import PlugAndPlay

            with PlugAndPlay() as p:
                p.print_devices()

    - **Addressing instruments by their hostname via (local) DNS.**
      This requires DHCP and a router with local DNS support. Note that most consumer-grade routers *don't* support this, but enterprise hardware usually does.

These four types of addresses (IP address, name, serial number, and DNS hostname) are collectively referred to as device identifiers. They are internally resolved via the :func:`~qblox_instruments.resolve` function; refer to its API documentation for the complete syntax supported by it. Both ``qblox-cfg`` and the Pulsar & Cluster instrument drivers in Python support all types of identifiers.

Verifying which instrument you're connected to
**********************************************

Once you have a connection, you might want to ensure that you're actually connected to the right instrument.
You can do this with the :meth:`~qblox_instruments.scpi.Cluster.identify` method on an instrument connection object: calling this will make the module blink its front-panel LEDs for a few seconds.
You can do the same from the command line using ``qblox-pnp identify <name-or-serial-number>``.
Alternatively, if you're not physically near the instrument but you do know what its name is supposed to be, you can use the :meth:`~qblox_instruments.scpi.Cluster.get_name` method to retrieve it from an open connection.

Renaming instruments
********************

The Cluster and Pulsar devices come pre-programmed with a default name.
For Clusters, this is ``cluster-cmm``, and for Pulsar QRMs and QCMs, this is ``pulsar-qrm`` and ``pulsar-qcm``, respectively.
You can change the name using :meth:`~qblox_instruments.scpi.Cluster.set_name` in Python, or using the ``set-name`` subcommand of ``qblox-cfg`` (see :ref:`getting_started/updating:IP address and name`).
Instrument names may be any single line of characters not including backslashes or underscores.

.. warning::

    Instrument names are case sensitive, so be aware that an instrument named ``A`` is different from a instrument named ``a``.

Hostnames have more stringent requirements. They must be lowercase, at most 63 characters long, and besides letters and numbers only the ``-`` (dash) is allowed. If you want to use local DNS, make sure that you configure a name that complies with these rules to avoid confusion. If you configure a name that isn't a valid hostname, the instrument will change its hostname to a best-effort representation of the invalid name.

.. note::

    Name changes go into effect immediately for resolution via :ref:`Qblox Plug & Play <api_pnp>`, but the hostname will only change after you reboot the instrument.

That's it! You are all set to start playing around with the Cluster or Pulsar hardware! We would recommend starting with the tutorials
available on this website (:ref:`tutorials/cont_wave_mode:Continuous waveform mode`) to get acquainted with the Python interface.


Connecting to an SPI Rack
-------------------------

In this section we will explain how to setup a `Qblox SPI Rack <https://www.qblox.com/spi>`_ and connect it to your PC.

Connecting to power
^^^^^^^^^^^^^^^^^^^

.. figure:: /figures/spi_power_connection_overview.svg
        :width: 400px
        :align: center
        :alt: An SPI Rack connected to a battery and via a gyrator to mains.

The recommended way to connect the SPI Rack to power is by connecting it via a gyrator to mains in parallel to a battery. This will ensure the system remains powered and stable, the gyrator prevents interference signals such as 50 Hz from getting to the SPI Rack by mimicking a large (~40H) inductor.


.. figure:: /figures/gyrator_and_batt_connect_schematic.svg
        :width: 700px
        :align: center
        :alt: An SPI Rack connected to a battery and via a gyrator to mains.

.. note::

    Please ensure there is a minimum of 1 meter between the gyrator and its power supply in order to effectively prevent interference signals from getting to the SPI Rack. Also ensure that the SPI Rack is not mounted in the same 19-inch rack with line-powered equipment, this includes the gyrator power supply.

Connecting to PC
^^^^^^^^^^^^^^^^

First, make sure a C1b controller module is fully inserted in the left-most slot of the SPI Rack. Then, connect the C1b to a C2 SPI Isolator box via a display port cable. Now, connect the C2 SPI Isolator box via USB to your PC. Finally, make sure the power switch on the back of the SPI Rack is in the `on` position, and the power supply is switched on as well. The LED on the C1b module should now turn on.

.. figure:: /figures/c1b_to_pc_connection.svg
        :width: 600px
        :align: center
        :alt: An SPI Rack connected to a battery and via a gyrator to mains.

On Windows systems, the SPI rack will now show up as a COM port in Windows Device Manager, while on OSX and Linux systems it will show up in the ``/dev/`` directory as ``/dev/ttyUSB#``, ``/dev/ttyS#`` or ``/dev/cu#`` (where ``#`` is a number). Please note down the COM port or ``/dev`` address as we will use it later.

We can now connect to the SPI Rack via the QCoDeS driver provided through the ``qblox-instruments`` package (see section :ref:`getting_started/setup:Installation`).

Communicating with the SPI Rack
*******************************

Starting the communication with the SPI Rack is done by simply instantiating an instance of the driver:

.. code-block:: python

    from qblox_instruments import SpiRack
    # Replace COM4 with the port that your SPI rack is connected to.
    # On OSX and Linux systems, this will be of the form /dev/tty## or /dev/cu##
    spi = SpiRack('SPI Rack', 'COM4')

SPI Modules
***********

To use an SPI Rack module, simply slide the module into one of the slots of the rack and fasten the screws. You should feel them "click" into place. The address on which to connect to the module is written on a sticker on the side of the module itself. Each of the modules we intend to use, we should add to the driver. This is done simply by:

.. code-block:: python

    spi.add_spi_module(2, "S4g") # example for an S4g on address 2

For more information on how to use the driver please visit the tutorial section (:ref:`tutorials/spi_rack:SPI Rack driver`).
