.. _handling:

Handling
========

Qblox instruments are sensitive pieces of equipment, which should not be opened under any circumstances. The Cluster contains
high-voltage parts which are dangerous. The end user shall not install substitute parts or perform any modification to the
product. Opening the instruments immediately voids the warranty provided by Qblox.

The safety instructions listed below should be observed during all operating procedures. Ignoring this might affect correct
operation and lifetime of the equipment.

+--------------------------------+--------------------------------------------------------------------------+
| Instruction                    | Explanation                                                              |
+================================+==========================================================================+
|| Maximum ratings               || The specified ratings for the connectors of the equipment should        |
||                               || not be exceeded.                                                        |
+--------------------------------+--------------------------------------------------------------------------+
|| Do not service the instrument || There are no serviceable parts inside the equipment. Only the battery   |
||                               || of a module is allowed to be replaced by the user.                      |
+--------------------------------+--------------------------------------------------------------------------+
|| Warnings                      || Warnings issued by the software should not be ignored, look for         |
||                               || instructions on how to handle these warnings in the documentation.      |
+--------------------------------+--------------------------------------------------------------------------+
|| Location and ventilation      || When placing the equipment, make sure the air in- and outlets are       |
||                               || kept free from obstruction to allow sufficient air to cool the devices. |
||                               || If there is other equipment with ventilation nearby, make sure that     |
||                               || the ventilation requirements do not interfere.                          |
+--------------------------------+--------------------------------------------------------------------------+
|| Cleaning                      || Disconnect the power supply from the equipment before cleaning.         |
||                               || The outside of the device can be cleaned using a soft cloth.            |
||                               || Do not clean the inside.                                                |
+--------------------------------+--------------------------------------------------------------------------+
|| Operation and storage         || Do not operate and store the equipment outside the specified            |
||                               || conditions.                                                             |
+--------------------------------+--------------------------------------------------------------------------+


.. note::
    Beware! The cluster modules can still be hot when extracted from the system in case the system was running shortly
    before extraction.


Placement
---------

Both the Pulsar and Cluster are meant for indoor use only. Both types can be used as a desktop device. The cluster can
also be mounted into a 19" rack.

The device should be positioned in such a way that the disconnecting device is easily reachable.

When placing the equipment make sure the air in- and outlets are kept free from obstruction to allow sufficient air to
cool the devices. In case the Cluster is mounted in a 19" rack one should make sure the total amount of air allowed to
flow is sufficient for all devices together. Other equipment installed in the same rack with ventilation requirements
should not interfere with the ventilation requirements of the Cluster.

SMA connections
---------------

The SMA connectors should be fixed with a maximum torque of 0.55 Nm. A torque wrench should
be used to make sure the maximum is never exceeded.

Ethernet connection
-------------------

The ethernet should always be connected with at shielded category 5 or 6 ethernet cable. For optimal performance, use
the included Cat6 S/FTP cables or similar ones.

Unusual conditions
------------------

If any of the conditions listed below are noticed, please power down the equipment immediately and contact the Qblox
support team.

+----------------------------------------------+-------------------------------------------------+
| Condition                                    | Explanation                                     |
+==============================================+=================================================+
|| Fan stopped working/is working incorrectly  || Turn off the equipment immediately to prevent  |
||                                             || overheating.                                   |
+----------------------------------------------+-------------------------------------------------+
|| The power cord/supply is damaged            || Turn off the equipment immediately. Exchange   |
||                                             || the power cord/supply only with the specified  |
||                                             || and certified product.                         |
+----------------------------------------------+-------------------------------------------------+
|| The equipment emits noise, smell or sparks. || Turn off the equipment immediately and prevent |
||                                             || further usage.                                 |
+----------------------------------------------+-------------------------------------------------+
