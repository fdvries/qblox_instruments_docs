.. _ip_addr:

Finding the IP address of a module
==================================

Here we provide some tips to help find the IP address of an instrument (e.g. a Pulsar QCM or QRM).

.. tip::
    When changing the IP address of the instrument, put a label on it with its new IP address. This will help avoid connectivity issues in the future.

Using plug & play
-----------------

The easiest way to find the IP address of an instrument or a number of instruments is via plug & play. It should work under the following circumstances.

 - The instrument firmware is at least at version 0.7.0.
 - You are directly connected to the instrument(s) via Ethernet, or there are only network switches in between. It will NOT work if there is a router or VPN connection in between. Follow steps 1-3 of :ref:`connecting` if possible. The IP address you configure for your Ethernet connection should not matter, as long as *some* IPv4 address is configured.

Now it should be as simple as running the following command in a terminal of your choice:

.. code-block:: console

    $ qblox-pnp list

That should give you something like this:

.. code-block:: console

    Devices:
     - 192.168.0.3: Pulsar QRM 0.7.0 with name "pulsar-qrm" and serial number 00013_2120_003

Using nmap
----------

``qblox-pnp`` won't be able to find your instruments across complex networks. For example, it won't work through a VPN. In this case, the nmap tool can help, although you will need to know which subnet your instruments are on (by default ``192.168.0.X``) to use it.

.. note::
    nmap will generally find all network devices on a network; it is not restricted to only Qblox instruments! If possible, disconnect all other devices that share the same network, such that only the host PC and the device with the an unknown IP are on the same subnetwork.

To use nmap, follow the instructions for your operating system.

MacOS/Linux
^^^^^^^^^^^
Open a terminal of your choice and run:

.. code-block:: console

    $ sudo nmap -sn 192.168.0.*

The output should look similar to:

.. code-block:: console

    Starting Nmap 7.91 ( https://nmap.org ) at 2021-01-02 20:16 CET
    Nmap scan report for 192.168.0.3
    Host is up (0.00029s latency).
    MAC Address: 04:91:62:BF:DE:57 (Microchip Technology)
    Nmap scan report for 192.168.0.200
    Host is up.
    Nmap done: 256 IP addresses (2 hosts up) scanned in 13.05 seconds

In this case our device has the IP :code:`192.168.0.3` while the network adapter of our host PC has been configured at :code:`192.168.0.200`.


Windows
^^^^^^^
On Windows we recommend installing the latest version of *nmap* that comes with a graphical interface (Zenmap).

1. Visit `download section at nmap.org <https://nmap.org/download.html>`_ and download the *Latest stable release self-installer* under the *Microsoft Windows binaries* section.
2. Run the installer with default options.
3. Execute **as administrator** the *Nmap - Zenmap GIU* that should appear on your Desktop (or in the start menu)
4. Type in the *Command* field :code:`nmap -sn 192.168.0.*` and hit *Enter* on your keyboard. After a few seconds the output should look similar to:

.. figure:: /figures/zenmap_windows.png
    :width: 800px
    :align: center
    :alt: Nmap - Zenmap GUI on windows

In this case our device has the IP :code:`192.168.0.3` while the network adapter of our host PC has been configured at :code:`192.168.0.240`.
