#------------------------------------------------------------------------------
# Description    : Configuration file for Sphinx documentation builder
#                  See https://www.sphinx-doc.org/en/master/usage/configuration.html for options
# Git repository : https://gitlab.com/qblox/packages/software/qblox_instruments_docs.git
# Copyright (C) Qblox BV (2020)
#------------------------------------------------------------------------------

import os
import sys

sys.path.insert(0, os.path.abspath("."))
sys.path.insert(0, os.path.abspath("../scripts"))
sys.path.insert(0, os.path.abspath("../qblox_instruments"))

import qblox_instruments
import qcodes_param


# -- Project information -----------------------------------------------------

project = "Qblox Instruments"
copyright = "2023, Qblox BV"
author = "Qblox"

# The full version, including alpha/beta/rc tags
release = qblox_instruments.__version__

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",  # auto document docstrings
    "sphinx.ext.napoleon",  # autodoc understands numpy docstrings
    "sphinx.ext.viewcode",
    "sphinx.ext.intersphinx",
    "sphinx.ext.autosectionlabel",
    "sphinx-jsonschema",
    "sphinx_rtd_theme",
    "sphinx_togglebutton",
    "sphinx.ext.mathjax",
    "nbsphinx",
    "jupyter_sphinx",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]


intersphinx_mapping = {
    "python": ("https://docs.python.org/3", None),
    "qcodes": ("https://qcodes.github.io/Qcodes/", None),
    "fastjsonschema": ("https://horejsek.github.io/python-fastjsonschema/", None)
}

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

# The master toctree document.
master_doc = "index"

# Default autodoc settings
autodoc_default_options = {
    "members": True,
    "show-inheritance": True,
    "member-order": "bysource",
    "special-members": "__init__",
    "undoc-members": True,
    "exclude-members": "__weakref__",
}

nitpick_ignore = [
    ("py:class", "datetime"),
    ("py:class", "BinaryIO"),
    ("py:class", "TextIO"),
    ("py:class", "Instrument"),
    ("py:class", "qcodes.instrument.base.Instrument"),
    ("py:class", "InstrumentChannel"),
    ("py:class", "Parameter"),
    ("py:exc", "JsonSchemaValueException"),
    ("py:class", "qcodes.instrument.parameter.Parameter"),
    ("py:class", "qblox_instruments.SystemState"),
    ("py:class", "qblox_instruments.SystemStatus"),
    ("py:class", "qblox_instruments.SystemStatusFlags"),
    ("py:class", "qblox_instruments.SystemStatusSlotFlags"),
    ("py:class", "qblox_instruments.SequencerState"),
    ("py:class", "qblox_instruments.SequencerStatus"),
    ("py:class", "qblox_instruments.SequencerStatusFlags"),
    ("py:class", "qblox_instruments.build.BuildInfo"),
    ("py:class", "qblox_instruments.build.DeviceInfo"),
    ("py:class", "qblox_instruments.types.InstrumentType"),
    ("py:class", "qblox_instruments.types.ClassType"),
    ("py:class", "qblox_instruments.types.TypeType"),
    ("py:class", "qblox_instruments.types.PulsarType"),
    ("py:class", "qblox_instruments.types.ClusterType"),
    ("py:class", "qblox_instruments.ieee488_2.ieee488_2.Ieee488_2"),
    ("py:class", "qblox_instruments.ieee488_2.transport.Transport"),
    ("py:class", "qblox_instruments.ieee488_2.ip_transport.IpTransport"),
    ("py:class", "qblox_instruments.ieee488_2.dummy_transport.DummyTransport"),
    ("py:class", "qblox_instruments.ieee488_2.dummy_transport.DummyBinnedAcquisitionData"),
    ("py:class", "qblox_instruments.ieee488_2.dummy_transport.DummyScopeAcquisitionData"),
    ("py:class", "qblox_instruments.ieee488_2.qcm_qrm_dummy_transport.QcmQrmDummyTransport"),
    ("py:class", "qblox_instruments.ieee488_2.pulsar_dummy_transport.PulsarDummyTransport"),
    ("py:class", "qblox_instruments.ieee488_2.cluster_dummy_transport.ClusterDummyTransport"),
    ("py:class", "qblox_instruments.scpi.pulsar_qcm.PulsarQcm"),
    ("py:class", "qblox_instruments.scpi.pulsar_qcm.PulsarQrm"),
    ("py:class", "qblox_instruments.scpi.cluster.Cluster"),
    ("py:class", "qblox_instruments.native.pulsar.Pulsar"),
    ("py:class", "qblox_instruments.native.cluster.Cluster"),
    ("py:class", "qblox_instruments.native.spi_rack.SpiRack"),
    ("py:class", "qblox_instruments.native.spi_rack_modules.spi_module_base.SpiModuleBase"),
    ("py:class", "qblox_instruments.native.spi_rack_modules.s4g_module.S4gModule"),
    ("py:class", "qblox_instruments.native.spi_rack_modules.d5a_module.D5aModule"),
    ("py:class", "qblox_instruments.qcodes_drivers.pulsar.Pulsar"),
    ("py:class", "qblox_instruments.qcodes_drivers.cluster.Cluster"),
    ("py:class", "qblox_instruments.qcodes_drivers.spi_rack.SpiRack"),
    ("py:class", "qblox_instruments.qcodes_drivers.spi_rack_modules.spi_module_base.SpiModuleBase"),
    ("py:class", "qblox_instruments.qcodes_drivers.spi_rack_modules.s4g_module.S4gModule"),
    ("py:class", "qblox_instruments.qcodes_drivers.spi_rack_modules.d5a_module.D5aModule"),
    ("py:class", "qblox_instruments.pnp.resolve.AddressInfo"),
]

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"
html_static_path = ['_static']
html_logo = "figures/qblox_block.png"
html_theme_options = {
    "display_version": False,
}
html_css_files = [
    'custom.css',
]

# -- Options for nbsphinx ----------------------------------------------------

nbsphinx_execute = "never"
nbsphinx_prolog = """
.. seealso::
    An IPython notebook version of this tutorial can be downloaded here:

    :download:`{{ env.docname[10:] + ".ipynb" }} <{{ env.docname[10:] + ".ipynb" }}>`
"""

# -- Options for autosectionlabel --------------------------------------------

autosectionlabel_prefix_document = True
