# ----------------------------------------------------------------------------
# Description    : Generate Pulsar QCoDeS parameters RST
# Git repository : https://gitlab.com/qblox/packages/software/qblox_instruments_docs.git
# Copyright (C) Qblox BV (2020)
# ----------------------------------------------------------------------------

# -- include -----------------------------------------------------------------

import os
import re

from typing import Union
from qcodes import Instrument, InstrumentChannel
from qblox_instruments import PulsarType, ClusterType, Pulsar, Cluster, SpiRack


# -- constants ---------------------------------------------------------------

BUILD_DIR = "../docs/_build"


# -- functions ---------------------------------------------------------------


def create_qcodes_param_rst(
    instrument: Union[Instrument, InstrumentChannel], name: str, filename: str
) -> None:
    """
    Get QCoDeS instrument parameters, parse them and write them in RST format
    to a file.

    Parameters
    ----------
    instrument : Union[Instrument, InstrumentChannel]
        Instrument or submodule to generate file for.
    name : str
        Unique component name.
    filename : str
        RST file name to generate.

    Returns
    ----------

    Raises
    ----------
    """

    # Parse all parameters
    file = open(filename, "w")
    for param in instrument.parameters.values():

        # Get info from doc string
        doc_str_list = param.__doc__.split("\n")
        doc_str_list = [doc_str_part.rstrip() for doc_str_part in doc_str_list]
        if doc_str_list[0] == "Parameter class:":
            param_doc_str_idx = None
            param_name_idx = 2
            param_unit_idx = 4
            param_val_idx = 5
        else:
            param_doc_str_idx = 0
            param_name_idx = 4
            param_unit_idx = 6
            param_val_idx = 7

        param_name = doc_str_list[param_name_idx].split("`")[2][1:]
        param_unit = doc_str_list[param_unit_idx].split("`")[2][1:]
        param_val = doc_str_list[param_val_idx].split("`")[2][2:-1]
        if param_doc_str_idx is not None:
            param_doc_str = doc_str_list[param_doc_str_idx]
        else:
            param_doc_str = "Please see " + \
                            "`QCoDeS <https://qcodes.github.io/Qcodes/>`_" + \
                            " for a description."

        # Write to file
        seq_idx_match = re.search("^sequencer([\d*]*).*", param_name)
        mod_idx_match = re.search("^module([\d*]*).*", param_name)
        if (
            (seq_idx_match is None and mod_idx_match is None) or
            (seq_idx_match is not None and seq_idx_match.group(1) == "0") or
            (mod_idx_match is not None and mod_idx_match.group(1) == "1")
        ):
            # Label
            param_name = name + "." + param_name
            file.write(".. _" + param_name + ":\n\n")

            # Parameter name
            header_line = f":code:`{param_name}`\n"
            under_line = "~" * (len(header_line) - 1) + "\n"
            file.write(header_line + under_line)

            # Doc string
            if param_doc_str != "":
                file.write("  " + param_doc_str + "\n")
            file.write("\n")

            # Properties
            file.write("  :Properties:\n")
            if param_unit != "":
                file.write("    - **unit**: " + param_unit + "\n")
            if param_val != "":
                file.write("    - **value**: " + param_val + "\n")
            file.write("\n")

    file.close()


# ----------------------------------------------------------------------------
def filepath(filename: str) -> str:
    """
    Create file path that places the file in the `_build` directory. Create
    directory if it doesn't exist yet.

    Parameters
    ----------
    filename : str
        File name to

    Returns
    ----------
    str
        filepath

    Raises
    ----------
    """

    script_dir = os.path.dirname(os.path.realpath(__file__))
    result_dir = os.path.join(script_dir, BUILD_DIR)
    if os.path.isdir(result_dir) is False:
        os.mkdir(result_dir)
    return os.path.join(result_dir, filename)


# -- main --------------------------------------------------------------------

# Create QCoDeS driver objects
clstr = Cluster(
    "Cluster",
    dummy_cfg={
        "1": ClusterType.CLUSTER_QCM,
        "2": ClusterType.CLUSTER_QCM_RF,
        "3": ClusterType.CLUSTER_QRM,
        "4": ClusterType.CLUSTER_QRM_RF,
    },
)
plsr_qcm = Pulsar("PulsarQcm", dummy_type=PulsarType.PULSAR_QCM)
plsr_qrm = Pulsar("PulsarQrm", dummy_type=PulsarType.PULSAR_QRM)
spi = SpiRack("SpiRack", "COM2", is_dummy=True)
spi.add_spi_module(1, "D5a", is_dummy=True)
spi.add_spi_module(2, "S4g", is_dummy=True)

# RST file definitions
rst_def = {"Cluster": [],
           "Pulsar": [],
           "SPI Rack": []}

rst_def["Cluster"].append({"device": clstr,
                           "name": "Cluster",
                           "file": "cluster_param.rst"})
rst_def["Cluster"].append({"device": clstr.module1,
                           "name": "(Cluster QCM) module",
                           "file": "cluster_qcm_param.rst"})
rst_def["Cluster"].append({"device": clstr.module2,
                           "name": "(Cluster QCM-RF) module",
                           "file": "cluster_qcm_rf_param.rst"})
rst_def["Cluster"].append({"device": clstr.module3,
                           "name": "(Cluster QRM) module",
                           "file": "cluster_qrm_param.rst"})
rst_def["Cluster"].append({"device": clstr.module4,
                           "name": "(Cluster QRM-RF) module",
                           "file": "cluster_qrm_rf_param.rst"})
rst_def["Cluster"].append({"device": clstr.module1.sequencer0,
                           "name": "(Cluster QCM) sequencer",
                           "file": "cluster_qcm_sequencer_param.rst"})
rst_def["Cluster"].append({"device": clstr.module2.sequencer0,
                           "name": "(Cluster QCM-RF) sequencer",
                           "file": "cluster_qcm_rf_sequencer_param.rst"})
rst_def["Cluster"].append({"device": clstr.module3.sequencer0,
                           "name": "(Cluster QRM) sequencer",
                           "file": "cluster_qrm_sequencer_param.rst"})
rst_def["Cluster"].append({"device": clstr.module4.sequencer0,
                           "name": "(Cluster QRM-RF) sequencer",
                           "file": "cluster_qrm_rf_sequencer_param.rst"})

rst_def["Pulsar"].append({"device": plsr_qcm,
                          "name": "(Pulsar QCM) Pulsar",
                          "file": "pulsar_qcm_param.rst"})
rst_def["Pulsar"].append({"device": plsr_qrm,
                          "name": "(Pulsar QRM) Pulsar",
                          "file": "pulsar_qrm_param.rst"})
rst_def["Pulsar"].append({"device": plsr_qcm.sequencer0,
                          "name": "(Pulsar QCM) sequencer",
                          "file": "pulsar_qcm_sequencer_param.rst"})
rst_def["Pulsar"].append({"device": plsr_qrm.sequencer0,
                          "name": "(Pulsar QRM) sequencer",
                          "file": "pulsar_qrm_sequencer_param.rst"})

rst_def["SPI Rack"].append({"device": spi,
                            "name": "SPIRack",
                            "file": "spi_rack_param.rst"})
rst_def["SPI Rack"].append({"device": spi.module1.dac0,
                            "name": "D5a",
                            "file": "d5a_param.rst"})
rst_def["SPI Rack"].append({"device": spi.module2.dac0,
                            "name": "S4g",
                            "file": "s4g_param.rst"})

# Generate RST files
script_dir = os.path.dirname(os.path.realpath(__file__))
result_dir = os.path.join(script_dir, BUILD_DIR)
if os.path.isdir(result_dir) is False:
    os.mkdir(result_dir)
for instrument in rst_def.values():
    for file_def in instrument:
        create_qcodes_param_rst(file_def["device"],
                                file_def["name"],
                                filepath(file_def["file"]))
