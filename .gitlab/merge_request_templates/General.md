### Explanation of changes

Describe the contents of this merge request and the issues addressed.

Add JIRA ticket ID to the MR title: (DOC-x)

--------------------

## Merge checklist

- [ ] Set merge request target branch: `new_outine` or `development`.
- [ ] Merge request has been reviewed by a R&D team member.
- [ ] Merge request has been reviewed by a commercial team member.
- [ ] Docs html build has been previewed.  
      `https://qblox-qblox-instruments--{MR number}.com.readthedocs.build/en/{MR number}`
